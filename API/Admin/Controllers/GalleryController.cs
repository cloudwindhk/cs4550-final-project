﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UnnoticedAPI.Models.Common;
using UnnoticedAPI.Models.Domain.Gallery;
using UnnoticedAPI.Models.View;
using UnnoticedAPI.Utils.Domain;
using UnnoticedAPI.Utils.Infrastructure;
using UnnoticedAPI.Utils.View;

namespace UnnoticedAPI.Controllers.MVC
{
    public class GalleryController : Controller
    {
        private static readonly int imagePageLimit = 10;
        private static readonly int imageSetPageLimit = 20;
        private static readonly object lockObj = new object();

        [HttpGet]
        [IsValidAuth]
        public ActionResult ProcessImages()
        {
            return View(GalleryAdminRepo.GetFirstUnprocessedImage());
        }

        [HttpPost]
        public ActionResult ProcessImages(ImageAdminDto dto, string authPassPhrase)
        {
            try
            {
                if (CommonUtils.IsValidAuth(HttpContext) || CommonUtils.IsValidAdminPhrase(authPassPhrase))
                    GalleryAdminRepo.UpdateImage(dto);

                if (!CommonUtils.IsValidAuth(HttpContext))
                    return RedirectToAction("Index", "Home");

                return RedirectToAction("ProcessImages");
            }
            catch (UnnoticedException ex)
            {
                TempData["neba_error"] = ex.Message;
                return View(dto);
            }
        }

        [HttpPost]
        public EmptyResult UploadImage(HttpPostedFileBase image)
        {
            if (!CommonUtils.IsValidAuth(HttpContext))
                return new EmptyResult();
            if (image != null && image.ContentLength > 0 && image.ContentType.Equals("image/jpeg"))
            {
                string fileName = System.IO.Path.GetFileName(image.FileName).Replace(" ", string.Empty);
                string serverLocalPathToRawUploads = HttpContext.Server.MapPath("~/Content/ImageUploads/Pending") + "/";
                System.IO.Directory.CreateDirectory(serverLocalPathToRawUploads);
                string filePath = serverLocalPathToRawUploads + fileName;
                filePath = filePath.Replace(".jpeg", ".jpg").Replace(".jpe", ".jpg");
                for (int j = 2; System.IO.File.Exists(filePath); j++)
                {
                    string tmpFilePath = filePath.Insert(filePath.LastIndexOf('.'), "." + j + ".jpg");
                    if (!System.IO.File.Exists(tmpFilePath))
                        filePath = tmpFilePath;
                }
                image.SaveAs(filePath);
            }
            return new EmptyResult();
        }

        [HttpPost]
        [IsValidAuth]
        public ActionResult UploadImages()
        {
            return RedirectToAction("ProcessUploadedImages");
        }

        [HttpGet]
        [IsValidAuth]
        public ActionResult ProcessUploadedImages()
        {
            if (Monitor.TryEnter(lockObj, 500))
            {
                try
                {
                    string serverLocalPathToRawUploads = HttpContext.Server.MapPath("~/Content/ImageUploads/Pending") + "/";
                    System.IO.DirectoryInfo rawDir = System.IO.Directory.CreateDirectory(serverLocalPathToRawUploads);
                    IEnumerable<System.IO.FileInfo> listOfFiles = rawDir.GetFiles().Where(fI => fI.Extension.ToLower().Contains("jpg") || fI.Extension.ToLower().Contains("jpe") || fI.Extension.ToLower().Contains("jpeg"));
                    foreach (System.IO.FileInfo imgFileInfo in listOfFiles)
                    {
                        string fileName = System.IO.Path.GetFileName(imgFileInfo.Name).Replace(" ", string.Empty);
                        string serverLocalPathToUploads = HttpContext.Server.MapPath("~/Content/ImageUploads/Processed") + "/";
                        string imagesDir = serverLocalPathToUploads + DateTime.UtcNow.ToString("MMMddyyyy") + "/";
                        System.IO.Directory.CreateDirectory(imagesDir);
                        string filePath = imagesDir + fileName;
                        filePath = filePath.Replace(".jpeg", ".jpg").Replace(".jpe", ".jpg");
                        for (int j = 2; System.IO.File.Exists(filePath); j++)
                        {
                            string tmpFilePath = filePath.Replace(".jpg", "." + j + ".jpg");
                            if (!System.IO.File.Exists(tmpFilePath))
                                filePath = tmpFilePath;
                        }
                        System.IO.File.Move(imgFileInfo.FullName, filePath);
                        using (System.Drawing.Image image = System.Drawing.Image.FromFile(filePath))
                        {
                            using (System.Drawing.Image cropThumb = ImageUtils.CropToThumb(image))
                            {
                                cropThumb.Save(filePath.Replace(".jpg", ".thumb.jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);
                            }
                            using (System.Drawing.Image cropDisp = ImageUtils.CropToDisplay(image))
                            {
                                cropDisp.Save(filePath.Replace(".jpg", ".disp.jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);
                            }

                            filePath = filePath.Replace(serverLocalPathToUploads, SiteConfigUtils.GetSiteRootUrl() + "Content/ImageUploads/Processed/");
                            Image toAdd = new Image
                            {
                                Aperture = ImageUtils.GetAperture(image),
                                Camera = ImageUtils.GetCamera(image),
                                Caption = "Uncaptioned",
                                DisplayPath = filePath.Replace(".jpg", ".disp.jpg"),
                                ThumbPath = filePath.Replace(".jpg", ".thumb.jpg"),
                                FilePath = filePath,
                                Exposure = ImageUtils.GetExposure(image),
                                FocalLength = ImageUtils.GetFocalLength(image),
                                FocalLengthIn35mm = ImageUtils.GetFocalLengthIn35mm(image),
                                Landscape = image.Width > image.Height,
                                Iso = ImageUtils.GetISO(image),
                                TakenAt = ImageUtils.GetDateTimeTaken(image)
                            };
                            GalleryAdminRepo.AddImage(toAdd);
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(lockObj);
                }
            }
            return RedirectToAction("ProcessImages");
        }

        [HttpGet]
        [IsValidAuth]
        public ActionResult EditImage(int id, int page = 1, int setId = -1)
        {
            return View(new ImageEditModel { ImgDto = GalleryAdminRepo.GetImage(id), Id = id, Page = page, SetId = setId });
        }

        [HttpPost]
        public ActionResult EditImage(ImageEditModel dto, string authPassPhrase)
        {
            try
            {
                if (CommonUtils.IsValidAuth(HttpContext) || CommonUtils.IsValidAdminPhrase(authPassPhrase))
                    GalleryAdminRepo.UpdateImage(dto.ImgDto);

                if (!CommonUtils.IsValidAuth(HttpContext))
                    return RedirectToAction("Index", "Home");

                string redirAction = "Images";
                RouteValueDictionary routeData = new RouteValueDictionary();
                if (dto.SetId > 0)
                {
                    redirAction = "ManageImageSet";
                    routeData.Add("id", dto.SetId);
                }
                if (dto.Page > 1)
                    routeData.Add("page", dto.Page);

                return RedirectToAction(redirAction, routeData);
            }
            catch (UnnoticedException ex)
            {
                TempData["neba_error"] = ex.Message;
                return View(dto);
            }
        }

        [HttpGet]
        [IsValidAuth]
        public ActionResult DeleteImage(int id, int page = 1, int setId = -1)
        {
            return View(new ImageDeleteModel { Id = id, Page = page, SetId = setId });
        }

        [HttpPost]
        public ActionResult DeleteImageConf(int id, int page, int setId, string authPassPhrase)
        {
            try
            {
                if (CommonUtils.IsValidAuth(HttpContext) || CommonUtils.IsValidAdminPhrase(authPassPhrase))
                {
                    DeleteImageFromDisk(id);
                    GalleryAdminRepo.DeleteImage(id);
                }

                if (!CommonUtils.IsValidAuth(HttpContext))
                    return RedirectToAction("Index", "Home");

                string redirAction = "Images";
                RouteValueDictionary routeData = new RouteValueDictionary();
                if (setId > 0)
                {
                    redirAction = "ManageImageSet";
                    routeData.Add("id", setId);
                }
                if (page > 1)
                {
                    int maxPage = setId > 0 ? GalleryAdminRepo.GetImagesOfSetMaxPage(setId, imagePageLimit) : GalleryAdminRepo.GetImagesMaxPage(imagePageLimit);
                    if (page > maxPage)
                        page = maxPage;
                    routeData.Add("page", page);
                }

                return RedirectToAction(redirAction, routeData);
            }
            catch (UnnoticedException ex)
            {
                TempData["neba_error"] = ex.Message;
                return RedirectToAction("DeleteImage", new { id = id });
            }
        }

        [NonAction]
        private void DeleteImageFromDisk(int id)
        {
            ImageAdminDto temp = GalleryAdminRepo.GetImage(id);
            System.IO.File.Delete(temp.FilePath.Replace(SiteConfigUtils.GetSiteRootUrl() + "Content/ImageUploads/Processed", HttpContext.Server.MapPath("~/Content/ImageUploads/Processed")));
            System.IO.File.Delete(temp.FilePath.Replace(SiteConfigUtils.GetSiteRootUrl() + "Content/ImageUploads/Processed", HttpContext.Server.MapPath("~/Content/ImageUploads/Processed")).Replace(".jpg", ".disp.jpg"));
            System.IO.File.Delete(temp.FilePath.Replace(SiteConfigUtils.GetSiteRootUrl() + "Content/ImageUploads/Processed", HttpContext.Server.MapPath("~/Content/ImageUploads/Processed")).Replace(".jpg", ".thumb.jpg"));
        }

        [HttpGet]
        [IsValidAuth]
        public ActionResult ImageSets(int page = 1)
        {
            return View(new ImageSetListModel { Page = page, MaxPage = GalleryAdminRepo.GetImageSetsMaxPage(imageSetPageLimit), ListData = GalleryAdminRepo.GetImageSets(imageSetPageLimit, page) });
        }

        [HttpGet]
        [IsValidAuth]
        public ActionResult CreateImageSet()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateImageSet(ImageSetDto dto, string authPassPhrase)
        {
            try
            {
                if (CommonUtils.IsValidAuth(HttpContext) || CommonUtils.IsValidAdminPhrase(authPassPhrase))
                    GalleryAdminRepo.AddImageSet(dto);

                if (!CommonUtils.IsValidAuth(HttpContext))
                    return RedirectToAction("Index", "Home");

                return RedirectToAction("ImageSets");
            }
            catch (UnnoticedException ex)
            {
                TempData["neba_error"] = ex.Message;
                return View();
            }
        }

        [HttpGet]
        [IsValidAuth]
        public ActionResult EditImageSet(int id)
        {
            return View(GalleryAdminRepo.GetImageSet(id));
        }

        [HttpPost]
        public ActionResult EditImageSet(ImageSetDto dto, string authPassPhrase)
        {
            try
            {
                if (CommonUtils.IsValidAuth(HttpContext) || CommonUtils.IsValidAdminPhrase(authPassPhrase))
                    GalleryAdminRepo.UpdateImageSet(dto);

                if (!CommonUtils.IsValidAuth(HttpContext))
                    return RedirectToAction("Index", "Home");

                return RedirectToAction("ImageSets");
            }
            catch (UnnoticedException ex)
            {
                TempData["neba_error"] = ex.Message;
                return View(dto);
            }
        }

        [HttpGet]
        [IsValidAuth]
        public ActionResult DeleteImageSet(int id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult DeleteImageSetConf(int id, string authPassPhrase)
        {
            try
            {
                if (CommonUtils.IsValidAuth(HttpContext) || CommonUtils.IsValidAdminPhrase(authPassPhrase))
                    GalleryAdminRepo.DeleteImageSet(id);

                if (!CommonUtils.IsValidAuth(HttpContext))
                    return RedirectToAction("Index", "Home");

                return RedirectToAction("ImageSets");
            }
            catch (UnnoticedException ex)
            {
                TempData["neba_error"] = ex.Message;
                return RedirectToAction("DeleteImageSet", new { id = id });
            }
        }

        [HttpGet]
        [IsValidAuth]
        public ActionResult ManageImageSet(int id, int page = 1)
        {
            return View(new ImageListModel { Page = page, MaxPage = GalleryAdminRepo.GetImagesOfSetMaxPage(id, imagePageLimit), ImageSetId = id, ListData = GalleryAdminRepo.GetImagesOfSet(id, imagePageLimit, page) });
        }
    }
}
