﻿using System.Collections.Generic;
using UnnoticedAPI.Models.Domain.Gallery;

namespace UnnoticedAPI.Models.View
{
    public class ImageSetListModel : CommonModel
    {
        public IEnumerable<ImageSetDto> ListData { get; set; }
    }
}