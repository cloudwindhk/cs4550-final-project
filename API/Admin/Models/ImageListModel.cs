﻿using System.Collections.Generic;
using UnnoticedAPI.Models.Domain.Gallery;

namespace UnnoticedAPI.Models.View
{
    public class ImageListModel : CommonModel
    {
        public IEnumerable<ImageAdminDto> ListData { get; set; }

        public int? ImageSetId { get; set; }
    }
}