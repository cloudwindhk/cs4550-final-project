﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UnnoticedAPI.Models.View
{
    public class ImageDeleteModel
    {
        public int Id { get; set; }
        public int Page { get; set; }
        public int SetId { get; set; }
    }
}