﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UnnoticedAPI.Models.Domain.Gallery;

namespace UnnoticedAPI.Models.View
{
    public class ImageEditModel : ImageDeleteModel
    {
        public ImageAdminDto ImgDto { get; set; }
    }
}