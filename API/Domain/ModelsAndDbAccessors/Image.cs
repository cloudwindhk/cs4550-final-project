﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UnnoticedAPI.Models.Domain.Gallery
{
    public class Image
    {
        public int Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string FilePath { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string ThumbPath { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string DisplayPath { get; set; }
        public bool Landscape { get; set; }
        public DateTime UploadedAt { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Caption { get; set; }
        public bool Private { get; set; }

        public DateTime? TakenAt { get; set; }
        public string Camera { get; set; }
        public double? Aperture { get; set; }
        public string Exposure { get; set; }
        public int? Iso { get; set; }
        public double? FocalLength { get; set; }
        public double? FocalLengthIn35mm { get; set; }

        public int? ImageSetId { get; set; }
    }
}