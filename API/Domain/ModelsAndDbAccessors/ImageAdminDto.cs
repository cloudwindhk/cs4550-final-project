﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace UnnoticedAPI.Models.Domain.Gallery
{
    public class ImageAdminDto
    {
        public int Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string FilePath { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string ThumbPath { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string DisplayPath { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Caption { get; set; }

        [DisplayName("Taken At")]
        public string TakenAt { get; set; }
        public string Camera { get; set; }
        public double? Aperture { get; set; }
        public string Exposure { get; set; }
        [DisplayName("ISO")]
        public int? Iso { get; set; }
        [DisplayName("Focal Length")]
        public double? FocalLength { get; set; }
        [DisplayName("Focal Length in 35mm")]
        public double? FocalLengthIn35mm { get; set; }

        [DisplayName("Parent Image Set")]
        public int? ImageSetId { get; set; }
    }
}