﻿using System.ComponentModel.DataAnnotations;

namespace UnnoticedAPI.Models.Domain.Gallery
{
    public class ImageSetDto
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        public bool Private { get; set; }

        public string ThumbPath { get; set; }
        public int Count { get; set; }
    }
}