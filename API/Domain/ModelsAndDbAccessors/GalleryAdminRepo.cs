﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UnnoticedAPI.Models.Common;
using UnnoticedAPI.Utils.Domain;

namespace UnnoticedAPI.Models.Domain.Gallery
{
    public class GalleryAdminRepo
    {
        public static void AddImage(Image dto)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    dto.UploadedAt = DateTime.Now;
                    context.Images.Add(dto);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static void DeleteImage(int id)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    context.Images.Remove(context.Images.Single(i => i.Id == id));
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static void UpdateImage(ImageAdminDto i)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    Image existingI = context.Images.Single(eI => eI.Id == i.Id);

                    existingI.FilePath = i.FilePath;
                    existingI.ThumbPath = i.ThumbPath;
                    existingI.DisplayPath = i.DisplayPath;
                    existingI.Caption = i.Caption;
                    if (i.ImageSetId.HasValue)
                    {
                        PingImageSet(i.ImageSetId.Value);
                        existingI.Private = GalleryAdminRepo.GetImageSet(i.ImageSetId.Value).Private;
                    }
                    else
                        existingI.Private = false;
                    if (i.TakenAt != null)
                        existingI.TakenAt = DateTime.Parse(i.TakenAt);
                    existingI.Camera = i.Camera;
                    existingI.Aperture = i.Aperture;
                    existingI.Exposure = i.Exposure;
                    existingI.Iso = i.Iso;
                    existingI.FocalLength = i.FocalLength;
                    existingI.FocalLengthIn35mm = i.FocalLengthIn35mm;
                    existingI.ImageSetId = i.ImageSetId;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static ImageAdminDto GetFirstUnprocessedImage()
        {
            using (UnnoticedDbContext context = new UnnoticedDbContext())
            {
                Image temp = context.Images.Where(i => !i.ImageSetId.HasValue).OrderBy(i => i.UploadedAt).FirstOrDefault();
                return temp != null ? GalleryAdminRepo.MapImageIntoDto(temp) : null;
            }
        }

        public static void AddImageSet(ImageSetDto dto)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    ImageSet addVal = new ImageSet()
                    {
                        Name = dto.Name,
                        Private = dto.Private,
                        LastUpdateAt = DateTime.UtcNow
                    };
                    context.ImageSets.Add(addVal);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static void DeleteImageSet(int id)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    ImageSet delVal = context.ImageSets.Include("Images").Single(iS => iS.Id == id);
                    foreach (Image img in delVal.Images)
                    {
                        img.ImageSetId = null;
                    }
                    context.ImageSets.Remove(delVal);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static void UpdateImageSet(ImageSetDto iS)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    ImageSet existingIS = context.ImageSets.Single(eIS => eIS.Id == iS.Id);

                    existingIS.Name = iS.Name;
                    existingIS.Private = iS.Private;
                    existingIS.LastUpdateAt = DateTime.UtcNow;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static void PingImageSet(int id)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    ImageSet existingIS = context.ImageSets.Single(eIS => eIS.Id == id);
                    existingIS.LastUpdateAt = DateTime.UtcNow;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static IEnumerable<SelectListItem> GenerateSelectListOfSets()
        {
            List<SelectListItem> retVal = new List<SelectListItem>();
            SelectListItem defaultSel = new SelectListItem();
            defaultSel.Text = "--Unassigned--";
            defaultSel.Value = "";
            retVal.Add(defaultSel);
            foreach (ImageSetDto dto in GalleryAdminRepo.GetImageSets(0, 1).OrderBy(iS => iS.Name))
            {
                SelectListItem temp = new SelectListItem();
                temp.Text = dto.Name;
                temp.Value = dto.Id.ToString();
                retVal.Add(temp);
            }
            return retVal;
        }

        public static ImageAdminDto MapImageIntoDto(Image i)
        {
            return new ImageAdminDto
            {
                Id = i.Id,
                FilePath = i.FilePath,
                ThumbPath = i.ThumbPath,
                DisplayPath = i.DisplayPath,
                Caption = i.Caption,
                TakenAt = i.TakenAt.HasValue ? CommonUtils.FormatDateTime(i.TakenAt.Value) : null,
                Camera = i.Camera,
                Aperture = i.Aperture,
                Exposure = i.Exposure,
                Iso = i.Iso,
                FocalLength = i.FocalLength,
                FocalLengthIn35mm = i.FocalLengthIn35mm,
                ImageSetId = i.ImageSetId
            };
        }

        public static IEnumerable<ImageAdminDto> MapImageListIntoDtos(IEnumerable<Image> iis)
        {
            return from i in iis
                   select MapImageIntoDto(i);
        }

        public static ImageAdminDto GetImage(int id)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    Image retVal = context.Images.Single(i => i.Id == id);
                    return MapImageIntoDto(retVal);
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static IEnumerable<ImageAdminDto> GetImages(int limit, int page)
        {
            return MapImageListIntoDtos(GalleryCommonRepo.GetImagesBase(limit, page, true)).ToList();
        }


        public static int GetImagesMaxPage(int limit)
        {
            return GalleryCommonRepo.GetImagesMaxPage(limit, true);
        }

        public static IEnumerable<ImageAdminDto> GetImagesOfSet(int id, int limit, int page)
        {
            return MapImageListIntoDtos(GalleryCommonRepo.GetImagesOfSetBase(id, limit, page, true)).ToList();
        }

        public static int GetImagesOfSetMaxPage(int id, int limit)
        {
            return GalleryCommonRepo.GetImagesOfSetMaxPage(id, limit, true);
        }

        public static ImageSetDto GetImageSet(int id)
        {
            try
            {
                return GalleryCommonRepo.GetImageSet(id, true);
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static IEnumerable<ImageSetDto> GetImageSets(int limit, int page)
        {
            try
            {
                return GalleryCommonRepo.GetImageSets(limit, page, true, true);
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static int GetImageSetsMaxPage(int limit)
        {
            try
            {
                return GalleryCommonRepo.GetImageSetsMaxPage(limit, true, true);
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }
    }
}