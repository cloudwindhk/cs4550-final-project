﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UnnoticedAPI.Models.Domain.Gallery
{
    public class ImageSet
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        public bool Private { get; set; }

        public DateTime LastUpdateAt { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}