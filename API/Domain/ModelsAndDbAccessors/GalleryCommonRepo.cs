﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UnnoticedAPI.Models.Common;
using UnnoticedAPI.Utils.Domain;

namespace UnnoticedAPI.Models.Domain.Gallery
{
    public class GalleryCommonRepo
    {
        public static IEnumerable<Image> GetImagesBase(int limit, int page, bool showPrivate)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    IEnumerable<Image> retVal = context.Images.Where(i => i.ImageSetId.HasValue);
                    if (!showPrivate)
                        retVal = retVal.Where(i => !i.Private);
                    retVal = retVal.OrderBy(i => i.TakenAt);
                    if (limit > 0)
                    {
                        if (page > 0)
                            retVal = retVal.Skip((page - 1) * limit).Take(limit);
                        else
                            retVal = retVal.Take(limit);
                    }
                    return retVal.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static int GetImagesMaxPage(int limit, bool showPrivate)
        {
            try
            {
                if (limit > 0)
                {
                    using (UnnoticedDbContext context = new UnnoticedDbContext())
                    {
                        IEnumerable<Image> filterCol = context.Images.Where(i => i.ImageSetId.HasValue);
                        if (!showPrivate)
                            filterCol = filterCol.Where(i => !i.Private);
                        return (int)Math.Ceiling((double)filterCol.Count() / limit);
                    }
                }
                else
                    return -1;
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static ImageSetDto MapSetIntoDto(ImageSet iS)
        {
            Random rand = new Random();
            return new ImageSetDto
            {
                Id = iS.Id,
                Name = iS.Name,
                Private = iS.Private,
                ThumbPath = iS.Images != null && iS.Images.Count > 0 ? iS.Images.ElementAt(rand.Next(iS.Images.Count)).ThumbPath : null,
                Count = iS.Images.Count
            };
        }

        public static IEnumerable<ImageSetDto> MapSetListIntoDtos(IEnumerable<ImageSet> iSs)
        {
            return from iS in iSs
                   select MapSetIntoDto(iS);
        }

        public static ImageSetDto GetImageSet(int id, bool showPrivate)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    ImageSet retVal = context.ImageSets.Include("Images").Single(iS => iS.Id == id);
                    if (!showPrivate && retVal.Private)
                        throw new UnnoticedException("This image set is private");
                    return MapSetIntoDto(retVal);
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static IEnumerable<ImageSetDto> GetImageSets(int limit, int page, bool isAdmin, bool showPrivate)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    IEnumerable<ImageSet> retVal = context.ImageSets.Include("Images");
                    if (!showPrivate)
                        retVal = retVal.Where(i => !i.Private);
                    if (!isAdmin)
                        retVal = retVal.Where(iS => iS.Images.Count() > 0);
                    retVal = retVal.OrderByDescending(iS => iS.LastUpdateAt);
                    if (limit > 0)
                    {
                        if (page > 0)
                            retVal = retVal.Skip((page - 1) * limit).Take(limit);
                        else
                            retVal = retVal.Take(limit);
                    }
                    return MapSetListIntoDtos(retVal).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static int GetImageSetsMaxPage(int limit, bool isAdmin, bool showPrivate)
        {
            try
            {
                if (limit > 0)
                {
                    using (UnnoticedDbContext context = new UnnoticedDbContext())
                    {
                        IEnumerable<ImageSet> filterCol = context.ImageSets.Include("Images");
                        if (!showPrivate)
                            filterCol = filterCol.Where(i => !i.Private);
                        if (!isAdmin)
                            filterCol = filterCol.Where(iS => iS.Images.Count() > 0);
                        return (int)Math.Ceiling((double)filterCol.Count() / limit);
                    }
                }
                else
                    return -1;
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static IEnumerable<Image> GetImagesOfSetBase(int id, int limit, int page, bool showPrivate)
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    if (!showPrivate && context.ImageSets.Single(iS => iS.Id == id).Private)
                        throw new UnnoticedException("This image set is private");

                    IEnumerable<Image> retVal = context.Images;
                    retVal = retVal.Where(i => i.ImageSetId.HasValue && i.ImageSetId.Value == id);
                    retVal = retVal.OrderBy(i => i.TakenAt);
                    if (limit > 0)
                    {
                        if (page > 0)
                            retVal = retVal.Skip((page - 1) * limit).Take(limit);
                        else
                            retVal = retVal.Take(limit);
                    }
                    return retVal.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static int GetImagesOfSetMaxPage(int id, int limit, bool showPrivate)
        {
            try
            {
                if (limit > 0)
                {
                    using (UnnoticedDbContext context = new UnnoticedDbContext())
                    {
                        if (!showPrivate && context.ImageSets.Single(iS => iS.Id == id).Private)
                            throw new UnnoticedException("This image set is private");
                        IEnumerable<Image> filterCol = context.Images.Where(i => i.ImageSetId.HasValue && i.ImageSetId.Value == id);
                        return (int)Math.Ceiling((double)filterCol.Count() / limit);
                    }
                }
                else
                    return -1;
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static string GetRandomBackgroundImage()
        {
            try
            {
                using (UnnoticedDbContext context = new UnnoticedDbContext())
                {
                    IEnumerable<Image> filterIt = context.Images;
                    List<string> urlsOfImages = (from i in filterIt
                                                 where i.Landscape && i.ImageSetId.HasValue && !i.Private
                                                 select i.DisplayPath).ToList();
                    if (urlsOfImages.Count > 0)
                        return urlsOfImages.ElementAt(new Random().Next(urlsOfImages.Count));
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }
    }
}