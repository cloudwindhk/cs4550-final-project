﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnnoticedAPI.Models.Common;
using UnnoticedAPI.Utils.Domain;

namespace UnnoticedAPI.Models.Domain.Gallery
{
    public class GalleryApiRepo
    {
        public static ImageApiDto MapImageIntoDto(Image i, int curIndex, int querySize, int perpagecount, int page)
        {
            bool hasNext = false;
            bool hasPrev = false;
            int normalizedIndex = curIndex + 1 + (perpagecount * (page - 1));
            if (querySize > 0)
            {
                if (normalizedIndex < querySize)
                    hasNext = true;
                if (curIndex > 0)
                    hasPrev = true;
            }

            return new ImageApiDto
            {
                Id = i.Id,
                QueryIndex = normalizedIndex,
                HasNext = hasNext,
                HasPrev = hasPrev,
                FilePath = i.FilePath,
                ThumbPath = i.ThumbPath,
                DisplayPath = i.DisplayPath,
                Caption = i.Caption,
                TakenAt = i.TakenAt.HasValue ? CommonUtils.FormatDateTime(i.TakenAt.Value) : null,
                Camera = i.Camera,
                Aperture = i.Aperture,
                Exposure = i.Exposure,
                Iso = i.Iso,
                FocalLength = i.FocalLength,
                FocalLengthIn35mm = i.FocalLengthIn35mm,
                ImageSetId = i.ImageSetId
            };
        }

        public static IEnumerable<ImageApiDto> MapImageListIntoDtos(IEnumerable<Image> iis, int perpagecount, int page)
        {
            try
            {
                return iis.Select((img, ind) => MapImageIntoDto(img, ind, iis.Count(), perpagecount, page));
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static ImageApiDto GetImage(int index, int perpagecount, bool authed)
        {
            try
            {
                return MapImageListIntoDtos(GalleryCommonRepo.GetImagesBase(0, 1, authed), perpagecount, 1).ToList().Single(i => i.QueryIndex == index);
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static IEnumerable<ImageApiDto> GetImages(int perpagecount, int limit, int page, bool authed)
        {
            try
            {
                return MapImageListIntoDtos(GalleryCommonRepo.GetImagesBase(limit, page, authed), perpagecount, page).ToList();
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static ImageApiDto GetImageOfSet(int setId, int index, int perpagecount, bool authed)
        {
            try
            {
                return MapImageListIntoDtos(GalleryCommonRepo.GetImagesOfSetBase(setId, 0, 1, authed), perpagecount, 1).ToList().Single(i => i.QueryIndex == index);
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }

        public static IEnumerable<ImageApiDto> GetImagesOfSet(int id, int limit, int perpagecount, int page, bool authed)
        {
            try
            {
                return MapImageListIntoDtos(GalleryCommonRepo.GetImagesOfSetBase(id, limit, page, authed), perpagecount, page).ToList();
            }
            catch (Exception ex)
            {
                throw new UnnoticedException(ex);
            }
        }
    }
}