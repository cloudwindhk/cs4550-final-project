﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UnnoticedAPI.Models.Common;
using UnnoticedAPI.Models.Domain.Gallery;
using UnnoticedAPI.Utils.Domain;

namespace UnnoticedAPI.Controllers.API
{
    public class ImageController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get(int index, int perpagecount = 12)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            String responseBody = null;

            string authVar = null;
            if (Request.Headers.Contains("NebaAuth"))
                authVar = Request.Headers.GetValues("NebaAuth").FirstOrDefault();
            bool authed = CommonUtils.IsValidPrivate(authVar);

            try
            {
                responseBody = JsonConvert.SerializeObject(new { CallSuccess = true, Payload = GalleryApiRepo.GetImage(index, perpagecount, authed) });
            }
            catch (UnnoticedException ex)
            {
                responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "No image found", Exception = ex.Message, Parameters = new { Index = index } });
            }

            response.Content = new StringContent(responseBody);
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetMultiple(int perpagecount = 12, int limit = 0, int page = 1)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            String responseBody = null;

            string authVar = null;
            if (Request.Headers.Contains("NebaAuth"))
                authVar = Request.Headers.GetValues("NebaAuth").FirstOrDefault();
            bool authed = CommonUtils.IsValidPrivate(authVar);

            try
            {
                IEnumerable<ImageApiDto> retVal = GalleryApiRepo.GetImages(perpagecount, limit, page, authed);
                if (retVal.Count() > 0)
                    responseBody = JsonConvert.SerializeObject(new { CallSuccess = true, Payload = retVal, MaxPage = GalleryCommonRepo.GetImagesMaxPage(limit, authed) });
                else
                    responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "No images found", Parameters = new { Limit = limit, Page = page } });
            }
            catch (UnnoticedException ex)
            {
                responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "Exception when retrieving images", Exception = ex.Message, Parameters = new { Limit = limit, Page = page } });
            }

            response.Content = new StringContent(responseBody);
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetRandomBackgroundImage()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            String responseBody = null;

            try
            {
                string retVal = GalleryCommonRepo.GetRandomBackgroundImage();
                if (retVal != null)
                    responseBody = JsonConvert.SerializeObject(new { CallSuccess = true, Payload = retVal });
                else
                    responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "No background images found" });
            }
            catch (UnnoticedException ex)
            {
                responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "Exception when fetching background image", Exception = ex.Message });
            }

            response.Content = new StringContent(responseBody);
            return response;
        }
    }
}
