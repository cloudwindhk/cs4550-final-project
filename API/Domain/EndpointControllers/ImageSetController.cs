﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UnnoticedAPI.Models.Common;
using UnnoticedAPI.Models.Domain.Gallery;
using UnnoticedAPI.Utils.Domain;

namespace UnnoticedAPI.Controllers.API
{
    public class ImageSetController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get(int id, int limit = 0, int perpagecount = 12, int page = 1)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            String responseBody = null;

            string authVar = null;
            if (Request.Headers.Contains("NebaAuth"))
                authVar = Request.Headers.GetValues("NebaAuth").FirstOrDefault();
            bool authed = CommonUtils.IsValidPrivate(authVar);

            try
            {
                IEnumerable<ImageApiDto> retVal = GalleryApiRepo.GetImagesOfSet(id, limit, perpagecount, page, authed);
                if (retVal.Count() > 0)
                    responseBody = JsonConvert.SerializeObject(new { CallSuccess = true, Payload = retVal, MaxPage = GalleryCommonRepo.GetImagesOfSetMaxPage(id, limit, authed), SetName = GalleryCommonRepo.GetImageSet(id, authed).Name });
                else
                    responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "No images exist", Parameters = new { Id = id, Limit = limit, Page = page } });
            }
            catch (UnnoticedException ex)
            {
                responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "Exception when retrieving images", Exception = ex.Message, Parameters = new { Id = id, Limit = limit, Page = page } });
            }

            response.Content = new StringContent(responseBody);
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetMultiple(int limit = 0, int page = 1)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            String responseBody = null;

            string authVar = null;
            if (Request.Headers.Contains("NebaAuth"))
                authVar = Request.Headers.GetValues("NebaAuth").FirstOrDefault();
            bool authed = CommonUtils.IsValidPrivate(authVar);

            try
            {
                IEnumerable<ImageSetDto> retVal = GalleryCommonRepo.GetImageSets(limit, page, false, authed);

                if (retVal.Count() > 0)
                    responseBody = JsonConvert.SerializeObject(new { CallSuccess = true, Payload = retVal, MaxPage = GalleryCommonRepo.GetImageSetsMaxPage(limit, false, authed) });
                else
                    responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "No image sets found", Parameters = new { Limit = limit, Page = page } });
            }
            catch (UnnoticedException ex)
            {
                responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "Exception while getting image sets", Exception = ex.Message, Parameters = new { Limit = limit, Page = page } });
            }
            response.Content = new StringContent(responseBody);
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetImage(int setId, int index, int perpagecount)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            String responseBody = null;

            string authVar = null;
            if (Request.Headers.Contains("NebaAuth"))
                authVar = Request.Headers.GetValues("NebaAuth").FirstOrDefault();
            bool authed = CommonUtils.IsValidPrivate(authVar);

            try
            {
                ImageApiDto retVal = GalleryApiRepo.GetImageOfSet(setId, index, perpagecount, authed);
                responseBody = JsonConvert.SerializeObject(new { CallSuccess = true, Payload = retVal });
            }
            catch (UnnoticedException ex)
            {
                responseBody = JsonConvert.SerializeObject(new { CallSuccess = false, Reason = "No image found", Exception = ex.Message, Parameters = new { SetId = setId, Index = index } });
            }

            response.Content = new StringContent(responseBody);
            return response;
        }
    }
}
