﻿using CestLaMe.Models;
using CestLaMe.Utils;
using System.Web.Mvc;

namespace CestLaMe.Controllers
{
    public class GalleryController : Controller
    {
        private static readonly int imagesPageLimit = SiteConfigUtils.GetImagesPerPage();
        private static readonly int imageSetsPageLimit = SiteConfigUtils.GetImageSetsPerPage();

        [HttpGet]
        public ActionResult Index(int page = 1)
        {
            return View(new GalleryCommonModel(imageSetsPageLimit, page));
        }

        [HttpGet]
        public ActionResult ImageSet(int id, int page = 1)
        {
            return View(new ImageSetModel(id, imagesPageLimit, page));
        }

        [HttpGet]
        public ActionResult ImageBySet(int setId, int index)
        {
            return View(new ImageBySetModel(setId, index));
        }
    }
}
