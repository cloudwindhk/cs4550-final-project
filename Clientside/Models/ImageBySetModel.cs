﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CestLaMe.Models
{
    public class ImageBySetModel : SingleImageModel
    {
        private int setId { get; set; }

        public new string UrlParamJsonData
        {
            get
            {
                return JsonConvert.SerializeObject(new { SetId = setId, Index = index });
            }
        }

        public new string ApiAppend
        {
            get
            {
                return "?setId=" + setId + "&" + base.ApiAppend.Substring(1);
            }
        }

        public ImageBySetModel(int setId, int index) : base(index)
        {
            this.setId = setId;
        }
    }
}