﻿using CestLaMe.Utils;
using Newtonsoft.Json;

namespace CestLaMe.Models
{
    public class GalleryCommonModel
    {
        protected int page { get; set; }
        protected int limit { get; set; }
        protected int perpagecount = SiteConfigUtils.GetImagesPerPage();

        public string UrlParamJsonData
        {
            get
            {
                return JsonConvert.SerializeObject(new { Page = page });
            }
        }

        public string ApiAppend
        {
            get
            {
                return "?limit=" + limit + "&page=" + page + "&perpagecount=" + perpagecount;
            }
        }

        public GalleryCommonModel(int limit, int page)
        {
            this.limit = limit;
            this.page = page;
        }
    }
}