﻿using Newtonsoft.Json;

namespace CestLaMe.Models
{
    public class ImageSetModel : GalleryCommonModel
    {
        public int Id { get; set; }

        public new string UrlParamJsonData
        {
            get
            {
                return JsonConvert.SerializeObject(new { Id = Id, Page = page });
            }
        }

        public new string ApiAppend
        {
            get
            {
                return "?id=" + Id + "&" + base.ApiAppend.Substring(1);
            }
        }

        public ImageSetModel(int id, int limit, int page) : base(limit, page)
        {
            this.Id = id;
        }
    }
}