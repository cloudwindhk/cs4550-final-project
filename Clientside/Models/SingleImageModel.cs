﻿using CestLaMe.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CestLaMe.Models
{
    public class SingleImageModel
    {
        protected int index { get; set; }
        protected int perpagecount = SiteConfigUtils.GetImagesPerPage();

        public string UrlParamJsonData
        {
            get
            {
                return JsonConvert.SerializeObject(new { Index = index });
            }
        }

        public string ApiAppend
        {
            get
            {
                return "?index=" + index + "&perpagecount=" + perpagecount;
            }
        }

        public SingleImageModel(int index)
        {
            this.index = index;
        }
    }
}