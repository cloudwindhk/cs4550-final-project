var image_tile_markup = '<div class="tileelement imagetile"><div class="elementwrap"><div class="elementoverlay"><a href="{0}"><div><span>View Image</span></div></a></div><div class="elementbody" style="background-image: url({1});"><div><span class="titleline">{2}</span><span class="descline">{3}</span></div></div></div></div>';
var imageset_tile_markup = '<div class="tileelement imagesettile"><div class="elementwrap"><div class="elementoverlay"><a href="{0}"><div><span>View Set</span></div></a></div><div class="elementbody" style="background-image: url({1});"><div><span class="titleline">{2}</span><span class="descline">{3}</span></div></div></div></div>';
var image_markup = '<div class="contentboxbody"><img src="{0}" alt="" /></div><div class="contentboxend" id="imagecaption">{1}</div>';

var gallery_root_url = root_url + 'Gallery/';

var imageset_root_url = gallery_root_url + 'ImageSet/';
var image_by_set_root_url = gallery_root_url + 'ImageBySet/';


var loadImageSets = function (data) {
    if (data.CallSuccess) {
        var markup_insert = '';
        $.each(data.Payload, function (index, i) {
            var url_to_imgset = imageset_root_url + '?id=' + i.Id;
            var url_to_thumb_img = i.ThumbPath;
            var desc = i.Count + ' Picture';
            if (i.Count > 1)
                desc += 's';

            markup_insert += formatString(imageset_tile_markup, url_to_imgset, url_to_thumb_img, i.Name, desc);
        });

        var imageset_tile_container = $('#imagesettilecontainer');
        imageset_tile_container.html(markup_insert);
        $('#dataloadingbox').hide();
        imageset_tile_container.show();
    }
    else {
        $('#dataloadingbox').text(data.Reason);
    }
}

var loadSetImages = function (data, setId) {
    if (data.CallSuccess) {
        var markup_insert = '';
        $.each(data.Payload, function (index, i) {
            var url_to_img = image_by_set_root_url + '?setId=' + setId + '&index=' + i.QueryIndex;
            var url_to_thumb_img = i.ThumbPath;
            var taken_at_str = 'Taken at ';
            if (i.TakenAt)
                taken_at_str += i.TakenAt;
            else
                taken_at_str += 'Unknown';

            markup_insert += formatString(image_tile_markup, url_to_img, url_to_thumb_img, i.Caption, taken_at_str);
        });

        var image_tile_container = $('#imagetilecontainer');
        image_tile_container.html(markup_insert);
        $('#dataloadingbox').hide();
        image_tile_container.show();
    }
    else {
        $('#dataloadingbox').text(data.Reason);
    }
}

var loadImage = function (data) {
    if (data.CallSuccess) {
        markup_insert = formatString(image_markup, data.Payload.DisplayPath, data.Payload.Caption);

        var image_body = $('#imagebody');
        image_body.html(markup_insert);
        $('#dataloadingbox').hide();
        image_body.show();
    }
    else {
        $('#dataloadingbox').text(data.Reason);
    }
}

var loadExifBox = function (data) {
    if (data.CallSuccess) {
        var taken_at_exif = $('#taken_at_exif');
        var camera_exif = $('#camera_exif');
        var aperture_exif = $('#aperture_exif');
        var exposure_exif = $('#exposure_exif');
        var iso_exif = $('#iso_exif');
        var focal_length_exif = $('#focal_length_exif');
        var focal_length_35mm_exif = $('#focal_length_35mm_exif');
        if (data.Payload.TakenAt)
            taken_at_exif.html(data.Payload.TakenAt);
        else
            taken_at_exif.html('Unknown');
        if (data.Payload.Camera)
            camera_exif.html(data.Payload.Camera);
        else
            camera_exif.html('Unknown');
        if (data.Payload.Aperture)
            aperture_exif.html('f/' + data.Payload.Aperture);
        else
            aperture_exif.html('Unknown');
        if (data.Payload.Exposure)
            exposure_exif.html(data.Payload.Exposure + ' sec.');
        else
            exposure_exif.html('Unknown');
        if (data.Payload.Iso)
            iso_exif.html(data.Payload.Iso);
        else
            iso_exif.html('Unknown');
        if (data.Payload.FocalLength)
            focal_length_exif.html(data.Payload.FocalLength + ' mm');
        else
            focal_length_exif.html('Unknown');
        if (data.Payload.FocalLengthIn35mm)
            focal_length_35mm_exif.html(data.Payload.FocalLengthIn35mm + ' mm');
        else
            focal_length_35mm_exif.html('Unknown');
        $('#downloadfullsize').attr('href', data.Payload.FilePath);
        $('#fbsharelink').attr('data-href', document.URL);
        $('#exifbox').show();
    }
}

var prepSetReturnNav = function (cur_params, max_per) {
    var set_return_nav = $('#setreturnnav');
    var url_replace = imageset_root_url + '?id=' + cur_params.SetId;
    if (cur_params.Index > max_per) {
        var modded_ind = Math.ceil(cur_params.Index / max_per);
        url_replace += '&page=' + modded_ind;
    }
    set_return_nav.attr('href', url_replace);
    set_return_nav.show();
}

var prepGalleryPagination = function (url, max_page, cur_params) {
    var cur_page = cur_params.Page;

    var page_url_append = '';
    if (cur_params.Id)
        page_url_append += '&id=' + cur_params.Id;
    if (page_url_append.length > 0)
        page_url_append = '?' + page_url_append.substring(1);

    var page_url_append_prev = page_url_append;

    if (cur_page < max_page) {
        var older_btn = $('#pagingnext');
        if (page_url_append.length > 0)
            page_url_append += '&page=' + (cur_page + 1);
        else {
            page_url_append = '?page=' + (cur_page + 1);
        }
        older_btn.attr('href', url + page_url_append);
        older_btn.show();
    }

    if (cur_page > 1) {
        var newer_btn = $('#pagingprev');
        if (page_url_append_prev.length > 0)
            page_url_append_prev += '&page=' + (cur_page - 1);
        else {
            page_url_append_prev = '?page=' + (cur_page - 1);
        }
        newer_btn.attr('href', url + page_url_append_prev);
        newer_btn.show();
    }
}

var prepImagePagination = function (url, data, cur_params) {
    var cur_index = cur_params.Index;

    var index_url_append = '';
    if (cur_params.SetId)
        index_url_append += '&setId=' + cur_params.SetId;
    if (index_url_append.length > 0)
        index_url_append = '?' + index_url_append.substring(1);

    var index_url_append_prev = index_url_append;

    if (data.Payload.HasNext) {
        var older_btn = $('#pagingnext');
        if (index_url_append.length > 0)
            index_url_append += '&index=' + (cur_index + 1);
        else {
            index_url_append += '?index=' + (cur_index + 1);
        }
        older_btn.attr('href', url + index_url_append);
        older_btn.show();
    }

    if (data.Payload.HasPrev) {
        var newer_btn = $('#pagingprev');
        if (index_url_append_prev.length > 0)
            index_url_append_prev += '&index=' + (cur_index - 1);
        else {
            index_url_append_prev += '?index=' + (cur_index - 1);
        }
        newer_btn.attr('href', url + index_url_append_prev);
        newer_btn.show();
    }
}

var prepSetImagePagination = function (data, cur_params) {
    prepImagePagination(image_by_set_root_url, data, cur_params);
}

var prepImageSetsPagination = function (max_page, cur_params) {
    prepGalleryPagination(gallery_root_url, max_page, cur_params);
}

var prepImageSetPagination = function (max_page, cur_params) {
    prepGalleryPagination(imageset_root_url, max_page, cur_params);
}