var wrapperr = null;
var hidden_wrap = false;
var anim_in_prog = false;
var bodyy = null;
var hide_toggle = null;

// credit: microsoft asp.net ajax
var formatString = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp('\\{' + i + '\\}', 'gm');
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}

var pingCurrentNavLink = function (cur_nav_name) {
    if (cur_nav_name) {
        $('#headermenu > a').each(function (index, anch) {
            if ($('span', anch).text().indexOf(cur_nav_name) > -1)
                anch.setAttribute('class', 'active');
        });
    }
}

var setBackgroundGiven = function (featured_pic_json) {
    var bg_url = '/Content/images/bg.jpg';
    if (featured_pic_json.CallSuccess)
        bg_url = featured_pic_json.Payload;

    $('<img alt="" />').attr('src', bg_url).load(function () {
        $('body').css('background-image', 'url(' + bg_url + ')');
        $('#bgloadingoverlay').fadeOut(1000);
    });
}

var toggleWrapper = function () {
    if (!anim_in_prog) {
        anim_in_prog = true;
        selectWrapper();
        hide_toggle.html('Transitioning...');
        if (hidden_wrap) {
            wrapperr.fadeIn(1000, function () {
                hide_toggle.html('Show the Background');
            });
        }
        else {
            if (bodyy.scrollTop() != 0) {
                bodyy.animate({ scrollTop: 0 }, 500, 'linear', function () {
                    wrapperr.fadeOut(1000, function () {
                        hide_toggle.html('Show the Content');
                    });;
                });
            }
            else
                wrapperr.fadeOut(1000, function () {
                    hide_toggle.html('Show the Content');
                });
        }
        hidden_wrap = !hidden_wrap;
        anim_in_prog = false;
    }
}

var selectWrapper = function () {
    if (!wrapperr)
        wrapperr = $('#wrapper');
    if (!bodyy)
        bodyy = $('body');
    if (!hide_toggle)
        hide_toggle = $('#hidetoggle');
}